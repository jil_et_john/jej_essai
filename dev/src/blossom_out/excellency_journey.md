# La démarche d'excellence

Dans le sport, il paraît évident pour tout le monde que l'hygiène et la discipline sont importants.
Pourquoi n'en est il pas de même sur la psychologie ou la sensibilité ? \
Pourquoi est-ce difficile d'envisager que nous ayons besoin d'hygiène et de discipline pour cultiver notre égo ou nos émotions ?

La "**démarche d'excellence**" traduit le concepte abstrait derrière lequel nous considèrerons l'hygiène et la discipline
caractéristiques à une recherche pleine et entière de la performance et du bien être sur chacun des plans qui constituent l'humain.


## La construction du soi

Au même titre que le sport pour le corps, la **démarche d'excellence** implique un don de soi, une dépense d'énergie, une recherche du mieux et de manière générale tout ce qui permet de "s'améliorer".

Cette notion "d'amélioration" est la clé. \
Comment évaluer un état meilleur qu'un autre ? \
Comment juger une bonne discipline ? Une bonne hygiène ?

Nous considérerons ici qu'il existe deux critères : le **bien-être** et la **performance**.

Sur le plan du corps, le bien-être passe par une certaine "disponibilité" physique (par exemple je peux courir pour avoir un bus sans mettre 10 minutes à m'en remettre), par la satiété, par l'absence de douleur, etc. \
La performance se mesure assez facilement dans le sport donc il ne paraît pas nécessaire de développer.

Sur le plan psychologique, le bien-être peut être associé à la sérennité, au sentiment d'êre à sa place, etc. \
La performance se retrouve plus dans la capacité à résoudre des problèmes, à faire des plans, à synthétiser des informations, à envisager d'autres contextes, à envisager différents points de vue, ...

Sur le plan émotionnel, le bien-être passe par la capacité à être traversé par des émotions sans s'attacher à elles, par l'absence de frustrations, par l'acceptation du Drame, etc. \
La performance est plus délicate à mesurer car elle est extrêment subjective par nature mais il est probable qu'on puisse mesurer la performance au travers de capacités sociales telles que le charisme ou l'empathie.

:point_up: Attention la performance à elle seule n'est pas garante d'un bon épanouissement (hygiène/discipline). Par exemple la diminution de l'espérance de vie des athlètes de haut niveau peut-être considérée comme une diminution de l'épanouissement du corps alors qu'il y a clairement une amélioration de la performance.

Ce constat montre toute la difficulté de la démarche d'excellence. Par de nombreux aspects, bien-être et performance sont des aspirations antinomiques et réussir à les réunir relève de la résolution d'un paradoxe subtile.
Une autre manière de percevoir ce paradoxe : faire un effort induit de la douleur qui elle même n'est pas vraiment compatible avec ce que le sens commun associe au bien-être.

Ainsi, on commence à comprendre que pour résoudre ce paradoxe il faut sortir de la limite du résultat pour évaluer une performance et de la limite de l'instant pour évaluer le bien-être.

L'application réussie de la démarche d'excellence à sa construction personnelle résulte dans le sentiment **d'épanouissement**.


## Épanouissement

Comme dans de nombreux phénomènes d'évolution et d'après les expériences collectées on peut distinguer deux types de processus : un épanouissement continue et un épanouissement brutal.

L'épanouissement continue traduit les progrès naturels réalisés par l'application de la discipline et de l'entraînement. \
Je fais un jogging tous les soirs, je pratique des exercices logiques, etc. Et ainsi je progresse petit à petit au grès de mes efforts et de l'efficacité de mes exercices.

Cet aspect ne présente pas beaucoup d'intérêt à développer car il apparaît assez naturel dans notre environnement culturel.

Par contre le processus d'épanouissement "brutal" que nous nomerons ici **palier** est beaucoup plus marginal.

**TODO : raconter une histoire plutot que faire une description froide / peut-être les deux ?** \
[...]

Il est probable que la majorité des personnes qui se soient investies dans la démarche d'excellence pour une pratique aient déjà vécu ce phénomène de transformation brutale.

Dans son état initial, l'individu vit une tension intérieure (son objectif, son intention, ...) et une résistance (les limites de ses capacités, l'adversité, ...). Il se sent dans un état instable et désagréable, frustrant peut-être.

À force de chercher, au hasard de ses essais, de ses rencontres, des conseils, il débloque un nouvel état de conscience et accède à une nouvelle capacité sur lui même.

Brutalement, il se retrouve capable d'appréhender avec son esprit de nouveaux éléments sur sa nature, sur les mécanismes qui l'animent. Il franchit alors le seuil auquel il se confrontait ce qui s'accompagne brutalement de plus de bien être et de meilleures performances.

:point_up: Il peut exister un effet rebond. \
Lorsque la perception change, il y a souvent un temps d'adpatation et parfois on peut avoir le sentiment d'être incapable de faire ce qu'on savait faire avant. Effectivement après la transformation principale d'un palier il faut parfois prendre le temps à réapprendre ce qu'on savait déjà mais en intégrant notre nouvelle sensibilité.

## Les outils

La prétention et l'intention.
Chercher en soi même.

L'humilité et l'attention.
Chercher dans notre environnement.

Le vide.


## Les pièges


La satisfaction et les certitudes 

Le détachement

L'orgueil 

L'égocentrisme

