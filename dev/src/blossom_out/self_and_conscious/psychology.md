# La Projection

## La Construction de l'égo

### Le soi

Qui est ce que je crois être ?\
Qui est ce que je voudrais être ?\
Qui est ce que je suis réellement ?

### Cohérence et Légitimité


## Les piliers de la Projection

### Le Doute

### Les Miroirs

### Le Jugement
La perception\
L'analyse\
les valeurs

## Quelques idées de pratiques

Les jeux de stratégie pour entraîner son esprit à construire de la cohérence entre un état initial et un état souhaité.

Le "vide" psychologique : libérer sa pensée et son esprit, trouver l'impulsion.
* l'absurde
* l'humour
* la rhétorique: apprendre à penser suivant plusieurs points de vue. 
* le jeu de rôle : apprendre à percevoir, à penser et à choisir suivant plusieurs points de vue.
