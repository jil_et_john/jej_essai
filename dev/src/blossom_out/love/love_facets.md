# Les facettes de l'Amour

## Amour par soi même

Ici nous développerons l'idée que l'Amour s'il existe en tant qu'idéal se déploie en nous au travers de nos propres facettes d'existence : la psychologie, l'affectif et le corps. Ainsi nous proposons un model de cet Amour en accord avec le modèle du Soi.

### L'amour psychologique : l'amour d'adulte

La projection.

https://www.youtube.com/watch?v=1eo6EY81h50

* cohérence et légitimité
* projection
* miroirs
* égo

### L'amour affectif : l'amour d'enfant

La contemplation.

* bien-être
* contemplation
* corps émotionnel

### ???




## Affection et attachement ?

Il est intéressant de constater que chacune de ces facettes n'a pas les même propriétés sociales, les même conséquences dans nos relations, dans nos attentes, nos actes ou nos réactions.

identifier la relation comme une entité indépendante brise le biais cognitif de l'égo dans la relation psychologique que l'observation et la critique induisent dans notre conception de la relation.

En somme, attribuer un Miroir indépendant à la relation pour la traitée comme une entité libère.