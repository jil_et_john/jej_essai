# Les masques de l'Amour

## L'Amour n'est pas la passion.

Le coeur s'emballe, le visage blêmit, un frisson, peut-être même un vertige, ...
Et puis la difficulté à dormir, des idées qui reviennent en boucle et qui nous empêche de rester concentré 5 minutes sur un même sujet, ...
Peut-être même un désir permanent comme un camé en manque.

Mais ça, c'est de la [peur](https://www.mes15minutes.com/anxiete/). On appelle ça passion parce qu'on l'associe au plaisir mais est-ce vraiment l'état de cet idéal que l'on aspire à nommer Amour ? 


## L'Amour n'est pas l'Empathie ni la tendresse

Pour décrire et comprendre l'empathie, laissons parler quelqu'un qui a fait une analyse pertinente du sujet : [Compatir avec un Taliban - DBY #66](https://www.youtube.com/watch?v=6s_zXFmWM6g)

Nous pourrons donc distinguer l'**empathie cognitive** que nous appellerons **compassion** (qui repose plus sur les mécanismes psychologiques de la projection) de l'**empathie affective** que nous appellerons l'**empathie** au sens stricte (qui repose plus sur les mécanismes émotionnels de la contemplation).

**Todo : Tendresse**

## L'Amour n'est pas la Confiance

La Confiance ? \
Ce que nous appellerons Confiance s'illustre dans l'intime par l'**Allégorie de l'escalier** :
> Je suis en haut d'un escalier, face à son sommet, le dos dans le vide. \
> Je sais qu'il y a quelqu'un derrière moi. \
> \
> Je dois me jeter en arrière. \
> Quelque chose en moi résiste : la peur de la douleur, l'angoisse de l'inconnu, la perte du contacte rassurant du sol, ... \
> Tout me pousse à me crisper, me retient de sauter dans le vide, me pousse à rester là où je suis. \
> \
> Pourtant je pourrais me libérer de toutes ces peurs. Il y a quelqu'un derrière moi. Si je lui offre ma Confiance, je pourrais sauter sans craintes. \
> \
> La ***Confiance*** est le sentiment qui me porte si je suis capable de sauter en arrière en toute sérénité. 

La **Confiance** ne garantit pas qu'il me rattrapera. Elle ne garantit pas que je ne serai pas blessé. Elle me permet "seulement" de sauter, de me libérer de mes peurs et des moyens que j'utilisais instinctivement pour les affronter.
Ainsi lorsque j'offre ma confiance à quelqu'un, je me sens libre et léger. Ce sentiment me donne les moyens de prendre des risques et il m'aide à devenir entreprenant.

Effectivement, la confiance ça se perd. Si la personne derrière moi ne me rattrape pas, je serai blessé. \
Réelle ou fantasmée, l'idée de cette blessure m'enferme dans la peur et dans le Doute. Elle me cloue au sol et m'empêche de bouger.

La **Confiance** est un bienfait que nous devrions chercher à nous offrir à nous même. Que ce soit par notre force, notre pertinence ou par celles de nos proches. \
Ainsi libérés, nous pouvons mieux prendre la responsabilité de notre propre Bonheur. \
La liberté gagnée dans cet espace intérieur nous aide à être Juste : prendre la bonne position, la bonne décision, la bonne émotion pour tendre vers celui que nous aspirons à être pour nous même et pour les autres.

Cette Confiance ne peut s'épanouir que dans l'Amour. Elle ne doit pas être construite dans le déni, l'orgueil ou l'égocentrisme. \
La Confiance n'est pas l'Amour mais si elle est mise en place en harmonie avec lui, elle résonne et lui donne un corps bien plus grand et une âme bien plus profonde. Que ce soit la Confiance en soi pour l'Amour de soi même ou la Confiance en l'autre pour l'Amour de l'autre.

Voici une citation de [Marianne Williamson](https://fr.wikipedia.org/wiki/Marianne_Williamson) qui illustre aussi très bien cette notion de Confiance. Un citation reprise dans le film [Coach Carter](https://www.youtube.com/watch?v=ircu63Gi0T0).

> Notre peur la plus profonde n'est pas d'être inapte. \
> Elle est que nous puissions être doté d'un pouvoir sans commune mesure. \
> C'est notre clarté, pas nos zones d'ombres, qui nous effraie. \
> Nous nous demandons "Qui suis-je pour être brillant, talentueux, fabuleux, splendide ?" \
> En fait, quelle place ne méritez vous pas ? Vous êtes enfant de Dieu. \
> On n'apporte rien au monde en se dévalorisant. \
> Il n'est pas éclairé de se faire plus petit que l'on est, simplement pour rassurer les autres autour de nous. \
> Nous sommes nés pour manifester la gloire de Dieu, présente en nous. \
> Nous sommes tous conçus pour briller, comme les enfants. \
> Ce n'est pas donné qu'à quelques-uns, c'est en nous tous. \
> En laissant briller notre propre lumière, nous donnons inconsciemment aux autres le pouvoir d'en faire autant. \
> Si nous nous libérons de notre propre peur, notre présence seule pourra aussi libérer les autres. 


## L'Amour n'est pas la Complicité

Se comprendre sans parler. Se retrouver sans planifier. Se sentir liés. Ressentir de la joie et de l'enthousiasme à partager des activités ensemble.

Harmonie.

Synchronicité.


## L'Amour n'est pas le désir

Est-il nécessaire de détailler ce point ? \
Mais vous même, derrière cet écran, êtes vous capable de distinguer votre amour et votre désir ? D'apprécier, de juger, de manipuler chacun d'entre eux distinctement ?



## Conclusion

Chacun de ces masques peut-être partagé ou non et induire d'immenses plaisirs comme de terribles frustrations.

Mais finalement, ils ne sont pas l'Amour idéal que nous recherchons.

Pourtant, n'en sont-ils pas des symptômes et des causes ?
Quoi de mieux que d'avoir confiance en quelqu'un, du désir ou encore de la complicité si on veut cultiver l'Amour qu'on lui porte ?
Tous ces "masques" ne sont pas l'Amour mais sans eux, l'Amour ne serait pas l'Amour et c'est bien dans l'Amour qu'ils deviennent sains et merveilleux.

Dans quelle mesure ces masques peuvent-ils nous aider à comprendre et à caractériser l'Amour ? \
Que pouvons nous apprendre d'eux sur nous même et sur les relations qui nous touchent ?
