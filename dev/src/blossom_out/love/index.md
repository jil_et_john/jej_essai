# L'Amour

Sommes nous réellement capable d'apprécier, de juger, d'évaluer ce qu'est l'Amour ?\
Sommes nous capables de le vivre et de l'indentifier dans ce que nous avons de plus intime ?\
Il est intéressant de savoir qu'en grec ancien, il existait plusieurs mots pour désigner ce que nous même nous pouvons parfois réunir sous le terme Amour : [Mots grecs pour dire amour](https://fr.wikipedia.org/wiki/Mots_grecs_pour_dire_amour)

La [définition proposée sur Wikipédia](https://fr.wikipedia.org/wiki/Amour) :
> L'amour désigne un sentiment intense d'affection et d'attachement envers un être vivant ou une chose qui pousse ceux qui le ressentent à rechercher une proximité physique, intellectuelle ou même imaginaire avec l'objet de cet amour. Dans le cas d'une autre personne, l'amour peut conduire à adopter un comportement particulier et aboutir à une relation amoureuse si cet amour est partagé.

fait ressortir les idées d'affection et d'attachement. Mais il est courant d'associer à l'Amour un "idéal", un axe fondateur de notre Légitimité.

L'objet de ce paragraphe est donc de créer des images et un vocabulaire qui nous permettent d'abord de critiquer et d'assainir notre relation psychologique à l'Amour. Mais aussi de trouver des clés pour transmettre, partager et vivre en pleine conscience l'expérience de cet Amour.

Nous chercherons donc à identifier ce que l'Amour n'est pas. Distinguer toutes les expériences et émotions qui peuvent parasiter notre perception de l'Amour.

Puis dans une deuxième partie, nous essaierons de caractériser l'Amour sur nos différents plans d'existence et d'épanouissement : la Projection sur le plan psychologique, la Contemplation sur le plan émotionnel, et ??? sur le plan physique.

---

**Todo** : 
* link **Légitimité**
* link **Projection**
* link **Contemplation**
* link **???**