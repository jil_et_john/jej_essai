---
sidebar: auto
---

# Vers une démarche réflexive et collaborative

Ce travail a été impulsé par un besoin viscéral de trouver du Sens, de s'épanouir, d'être heureux, d'aimer et d'être aimer, ...

Il n'a pas vocation à devenir un dogme, une vérité ou quoi que ce soit de cet ordre.

Ce site est créé à partir d'un dépot git, proposez des modification et débattons :)

**Todo**

# Références, remerciements, inspiration


Todo : à regarder / compléter / revoir

* [Benjamin bayart](https://fr.wikipedia.org/wiki/Benjamin_Bayart)
    https://www.youtube.com/watch?v=VBsLSfPs2PE
* [La quadrature du net](https://www.laquadrature.net/)
* [Franck Lepage](https://fr.wikipedia.org/wiki/Franck_Lepage)
    https://www.youtube.com/watch?v=X5BAozcoY8s
* [Framasoft](https://framasoft.org/)
* [Éloge de l'oisiveté](https://www.youtube.com/watch?v=mQaUNWJOqos)
* [L'argent dette](https://www.youtube.com/watch?v=YMOFBioFNvc)
* [Les quatre accords Toltèques](https://editions-jouvence.com/livre/les-quatre-accords-tolteques-2/) et [la maîtrise de l'amour](https://editions-jouvence.com/livre/la-maitrise-de-lamour-poche/)
* [Liberté et compagnie](https://www.fayard.fr/documents-temoignages/liberte-cie-9782213662817)
* [Wikipedia](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:%C3%80_propos_de_Wikip%C3%A9dia)
* [DirtyBiology](https://www.youtube.com/user/dirtybiology)

...

