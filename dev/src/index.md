Après 20 ans d'observations, d'écoute, d'analyse, de tests et de réflexion. Il est temps de prendre le clavier et de structurer ce travail dans une forme intelligible.

Rien de ce qui ne sera dit ici n'est à considérer comme une vérité.
La vérité et ce qu'elle apporte comme dérive dogmatique est l'ennemi de la démarche qui a fait naître ces textes, ces modèles, ces analyses, ces craintes et ces espoirs.

Cet essai est une photographie à un instant donné d'une vision, d'une perception, d'une compréhension du monde dans lequel l'humanité évolue.

Cette vision n'a jamais été vraie, elle ne le sera jamais. 
Elle aspire seulement à nous rapprocher du réel, du juste et de cet ensemble que l'on nomme humanité pour nous aider à construire individuellement notre critique, nos intentions, et à faire nos choix dans des délais de réflexion raisonables.

Son aboutissement ne saurait être que de servir de tremplin et de paillasson à la prochaine marche qui nous rapprochera de cet idéal que nous ne savons pas encore rêver.

Cet essai est le résultat d'une rage, d'une révolte, d'une envie profonde de comprendre ce monde pour pouvoir lui donner une direction et une trajectoire plus satisfaisantes pour l'humain.

20 ans de travail pour écrire quelques dizaines de pages jetées au hasard de cet océan numérique qu'est internet.

Ce message parviendra-t-il à quelqu'un ? \
Sera-t-il lu ? \
Apprécié ? \
Critiqué ?

Quelqu'un grandira-t-il grâce à lui ? \
Quelqu'un perfectionnera-t-il son amour grâce à lui ?

...