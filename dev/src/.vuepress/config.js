const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Éssai de l\'ère numérique',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  // Todo : use environment variables properly !

  // Dev hosting parameters
  host: '0.0.0.0',
  port: 8021,

  // Prod hosting parameters
  base: '/jej_essai/',

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: true,
    search: false,
    nav: [
      {
        text: 'Construction personnelle',
        link: '/blossom_out/'
      },
      {
        text: 'État des lieux de notre civilisation',
        link: '/society/',
      },
      {
        text: 'Une démarche collaborative',
        link: '/community/',
      },
    ],
    sidebar: {
      '/blossom_out/': [
        {
          title: 'Construction personnelle et épanouissement',
          path: '/blossom_out/',
          collapsable: false,
          children: [  
            {
              title: 'Qui suis-je ?',
              path: '/blossom_out/self_and_conscious/',
              collapsable: true,
              children: [
                '/blossom_out/self_and_conscious/psychology',
                '/blossom_out/self_and_conscious/emotion',
                '/blossom_out/self_and_conscious/body',
              ]
            },  
            {
              title: 'L\'Amour',
              path: '/blossom_out/love/',
              collapsable: true,
              children: [
                '/blossom_out/love/love_masks',
                '/blossom_out/love/love_facets',
              ]
            },
            '/blossom_out/excellency_journey',
            '/blossom_out/involvement',
            {
              title: 'Soi et les autres',
              path: '/blossom_out/self_and_group/',
              collapsable: true,
              children: [
                '/blossom_out/self_and_group/education_and_manipulation',
                '/blossom_out/self_and_group/dependency',
                '/blossom_out/self_and_group/law_principle_system'
              ]
            }
          ]
        }
      ],
      '/society/': [
        {
          title: 'État des lieux de notre civilisation',
          path: '/society/',
          collapsable: false,
          children: [
            '/society/sixth_extinction',
            '/society/love/',
            '/society/art_and_culture',
            {
              title: 'Les biais cognitifs qui ancrent notre réalité',
              path: '/society/cognitive_bias/',
              collapsable: true,
              children: [
                '/society/cognitive_bias/science_dogma',
                '/society/cognitive_bias/judgement',
                '/society/cognitive_bias/monetary_algebra',
              ]
            },
            {
              title: 'Le système économique',
              path: '/society/economic_system/',
              collapsable: true,
              children: [
                '/society/economic_system/architecture_analysis',
              ]
            },
            '/society/pyramidal_society',
            '/society/corporatist_totalitarianism',
            '/society/conspiracism_trap',
          ]
        }
      ],
      '/community/': [
        '',
        {
          title: 'Une démarche collaborative',
          collapsable: true,
          children: [
            '',
          ] 
        }
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
