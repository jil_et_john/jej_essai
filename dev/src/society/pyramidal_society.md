# L'humanité converge vers une pyramide mondiale centrée sur l'argent / le bénéfice / le profit.

Cette pyramide cultive l'orgueil et l'égocentrisme pour garantir sa pérénité hiérarchique. \
Cette pyramide utilise des "artifices" culturels pour maintenir son statut :
* récit national
* rêve américain
* l'homme est un loup pour l'homme
* ...
