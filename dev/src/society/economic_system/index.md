# Le système économique 


## sa mission et sa légitimité dans une société : la paix, la prospérité

* organisation et partage de la production
* institution de la confiance
* l'équité et le mérite


## son impacte concret

* cultive et amplifie les inégalités
* ne reconnaît aucune valeur telle que le bien commun, l'environnement, l'équité, l'épanouissement et la liberté individuel, etc.
* Nous sommes face à une crise majeure qui pourrait avoir des conséquences désastreuses ... (démultiplication de la famine, la misère, des guerres, ...) 
