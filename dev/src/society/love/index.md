# L'humain ne sait plus aimer

**Todo**

## Le déficit d'amour d'adulte 

* Un besoin identitaire fort 
    > montée des nationalismes
    > radicalisation des extrémistes religieux
    > explosion des mouvements de revendications identitaires
    > ...    
* Le fonctionnement des réseaux sociaux perverti l'amour d'adulte en le solicitant et en cultivant ses pièges

## La perversion de l'amour d'enfant

* La chimie et la drogue
* Égémonie des instincts primaires : surconsommation, perte de repères, ...

## La confusion vient aussi de l'incapacité à gérer la souffrance (le doute et le drame).
* Dogmatisation de la science au détriment de la démarche scientifique
* Confusion entre émotion / légitimité / empathie / projection / amour / désir / confiance / ...

