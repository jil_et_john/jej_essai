# Le risque du totalitarisme corporatiste

Historiquement, la pyramide actuelle s'est imposée durant la révolution industrielle sur la base du nationalisme. \
Internet a amorcé une révolution comparable à la révolution industrielle et à la révolution française qui en a découlé. \
Aujourd'hui le dernier temple "opérationnel" de la pyramide est l'entreprise. \
La pyramide devrait donc naturellement essayer de transformer notre monde nationaliste en un monde corporatiste pour assurer sa pérénité.
