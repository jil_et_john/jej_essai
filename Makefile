# Useful to ensure files produced in volumes over docker-compose exec
# commands are not "root privileged" files.
# export CURRENT_UID=$(shell id -u):$(shell id -g)

export DOCKER_WORKING_DIR=/var/www/jej-essai
export JEJ_SRC_DIR=$(shell realpath ./dev/)

ifndef DOCKER_BUILD_IMG
export DOCKER_BUILD_IMG=jej-js-build:local
endif

ifndef DOCKER_COMPOSE
export DOCKER_COMPOSE := docker-compose \
	-f infra/docker/build.docker-compose.yaml
endif

ifndef EXEC_JEJ
# EXEC_JEJ := ${DOCKER_COMPOSE} exec -u ${CURRENT_UID} jej-build sh -c
export EXEC_JEJ := ${DOCKER_COMPOSE} exec jej-build sh -c
endif


## Infra
#########

infra-init: docker-build infra-run dev-init
# yarn init
# yarn add -D vuepress

infra-run:
	${DOCKER_COMPOSE} up -d --remove-orphans

# stop containers and processes
infra-stop:
	${DOCKER_COMPOSE} stop

# infra-down is used to clean project and docker environment
infra-down:
	${DOCKER_COMPOSE} down --volumes
	make clean


## Dev
#######

dev-serve:
	${EXEC_JEJ} 'yarn dev'

dev-build:
	${EXEC_JEJ} 'yarn build'
	mv ${JEJ_SRC_DIR}/src/.vuepress/dist ./public
# TODO : replace this `mv` by proper setup (environment variable + https://v1.vuepress.vuejs.org/config/#dest)

dev-init:
	${EXEC_JEJ} 'yarn install'


## Clean
#########

fix-docker:
	sudo chown -R ${USER}:${USER} .

clean: fix-docker
	# rm node_modules & dist & public
	- docker image rm ${DOCKER_BUILD_IMG}
	

## Shell
#########

shell-build:
	${EXEC_JEJ} '/bin/sh'


## Docker
##########

docker-build:
	docker build --no-cache --build-arg working_dir=${DOCKER_WORKING_DIR} -t ${DOCKER_BUILD_IMG} -f ./infra/docker/build/Dockerfile .
	# docker build --build-arg working_dir=${DOCKER_WORKING_DIR} -t ${DOCKER_BUILD_IMG} -f ./infra/docker/build/Dockerfile .
	