# Jej essai

Ce projet sert à créer le site statique qui accueillera
l'**[essai de l'ère numérique](https://jil_et_john.gitlab.io/jej_essai/)**

## installation et développement

Ce projet est fait pour fonctionner avec les commandes définies
dans le **Makefile** et **Docker**


## Todo

* Créer un composant / un style dédié pour les conclusions ?
* Ajouter des dates aux MAJ des articles : https://vuepress.vuejs.org/plugin/official/plugin-last-updated.html#usage